M = {}

function M.toggleEOLchar(char)
	local line = vim.api.nvim_get_current_line()
	local last_char = line:sub(-1)
	if last_char == char then
		line = line:sub(1, -2)
	else
		line = line .. char
	end
	vim.api.nvim_set_current_line(line)
end

return M

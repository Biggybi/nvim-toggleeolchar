local M = {}
local options = require("toggleEOLchar.config").options

function M.createKeymaps()
	if options.create_keymaps == false then
		return
	end
	vim.keymap.set("n", options.map_prefix, M.keymap, { desc = "Toggle EOL char" })
end

function M.keymap()
	local charcode = vim.fn.getchar()
	if type(charcode) == "number" then
		charcode = vim.fn.nr2char(charcode)
	end
	if options.notify then
		vim.notify("charcode", charcode)
	end
	require("toggleEOLchar.toggleEOLchar").toggleEOLchar(charcode)
end

return M

local M = {}

function M.createCommand()
	vim.api.nvim_create_user_command("ToggleEOLchar", function(args)
		require("toggleEOLchar.toggleEOLchar").toggleEOLchar(args.args)
	end, { nargs = "*" })
end

return M

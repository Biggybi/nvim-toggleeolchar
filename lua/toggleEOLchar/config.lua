local M = {}

local defaults = {
	create_keymaps = true,
	map_prefix = "ga",
	notify = true,
}

M.options = defaults

M.setup = function(user_options)
	M.options = vim.tbl_extend("force", defaults, user_options or {})
end

return M

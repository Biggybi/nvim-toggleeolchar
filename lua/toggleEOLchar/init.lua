local M = {}

function M.setup(user_options)
	require("toggleEOLchar.config").setup(user_options)
	require("toggleEOLchar.command").createCommand()
	require("toggleEOLchar.keymap").createKeymaps()
end

return M
